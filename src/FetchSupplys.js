const FetchSupplys = () => {
  return (
    [
      {
        id: 0,
        name: '維生素C',
        completed: false,
      },
      {
        id: 1,
        name: 'B群',
        completed: false,
      },
      {
        id: 2,
        name: '魚油',
        completed: false,
      },
      {
        id: 3,
        name: '益生菌',
        completed: false,
      },
      {
        id: 4,
        name: '高蛋白',
        completed: false,
      },
    ]
  );
};

export default FetchSupplys;

