// src/components/Layout.js
import React, { useState } from 'react';
import { Container, Row, Col, Button, Navbar, Nav } from 'react-bootstrap';
import './Layout.css';
import HabbitCat from './HabbitCat';
import Calendar from './Calendar';

const Layout = ({ children }) => {
  const [sidebarOpen, setSidebarOpen] = useState(true);

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  };

  return (
    <div className={sidebarOpen ? '' : 'collapsed'}>
      {/* <Navbar bg='light' expand="lg"> */}
      <Navbar className='navbar' >
        {/* <Navbar.Brand href="#">MyApp</Navbar.Brand> */}
        <Button variant="outline-primary" onClick={toggleSidebar}>
          <img
            src={process.env.PUBLIC_URL + '/nav-bar-toggle-icon.svg'}
            width="20"
            height="20"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Button>
        <Navbar.Brand href="#" style={{ marginLeft: '20px', color: 'white'}}>Habits Tracker</Navbar.Brand>
      </Navbar>
      <Container fluid>
        <Row>
          {sidebarOpen && (
            // {/* <Col xs={2} id="sidebar" className="bg-light"> */}
            <Col xs={2} id="sidebar">
              <Nav className="flex-column">
                <HabbitCat></HabbitCat>
              </Nav>
            </Col>
          )}
          <Col xs={sidebarOpen ? 10 : 12} id="main-content">
            {children}
            <Calendar className='calendar'></Calendar>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Layout;

