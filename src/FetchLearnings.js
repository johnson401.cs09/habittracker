const FetchLearnings = () => {
  return (
    [
      {
        id: 0,
        name: 'leetcode x10',
        completed: false,
      },
      {
        id: 1,
        name: 'Git Tutorials',
        completed: false,
      },
      {
        id: 2,
        name: 'Code force',
        completed: false,
      },
      {
        id: 3,
        name: 'CI/CD tutorials',
        completed: false,
      },
      {
        id: 4,
        name: 'React tutorials',
        completed: false,
      },
    ]
  );
};

export default FetchLearnings;

