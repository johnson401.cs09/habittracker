import FetchTodos from './FetchTodos';
import { FormControl, InputGroup } from 'react-bootstrap';
import { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function TodoItem (props) {
  return (
    <InputGroup>
      <InputGroup.Checkbox defaultChecked={props.completed}></InputGroup.Checkbox>
      <FormControl
        defaultValue={props.name}
        style={{
          textDecoration: props.completed ? "line-through 2px" : "none",
        }}
      >
      </FormControl>
      <Button variant="outline-danger" onClick={props.onDelete}>
        <FontAwesomeIcon icon={faTrash}></FontAwesomeIcon>
      </Button>
    </InputGroup>
  )
}

const TodoList = () => {
  const [todos, setTodos] = useState( FetchTodos() );

  return (
    <Container>
      {todos.map((item) => (
        <TodoItem
          key={item.id}
          name={ item.name }
          completed={ item.completed }
          onDelete={
            () => {
              setTodos(
                todos.filter((x) => x.id !== item.id)
              )
            }
          }
        ></TodoItem>
      ))}
    </Container>
  );
};

export default TodoList;
