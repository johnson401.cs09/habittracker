const FetchTodos = () => {
  return (
    [
      {
        id: 0,
        name: '伏地挺身',
        completed: false,
      },
      {
        id: 1,
        name: '深蹲',
        completed: false,
      },
      {
        id: 2,
        name: '啞鈴前平舉',
        completed: false,
      },
      {
        id: 3,
        name: '臥推',
        completed: false,
      },
      {
        id: 4,
        name: '仰臥起坐',
        completed: false,
      },
    ]
  );
};

export default FetchTodos;

