FROM node:alpine

# Set working directory
WORKDIR /app

# Install dependencies
COPY package.json package-lock.json ./
RUN npm install

# Copy application code
COPY . .
EXPOSE 3000

# Start the application
CMD ["npm", "start"]

